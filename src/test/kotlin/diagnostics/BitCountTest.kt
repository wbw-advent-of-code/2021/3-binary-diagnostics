package diagnostics

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class BitCountTest {
    @ParameterizedTest
    @MethodSource("mostCommonSrc")
    internal fun testMostCommon(zeroes: Int, ones: Int, expected: Int) {
        assertEquals(expected, BitCount(zeroes, ones).mostCommon())
    }

    companion object {
        @JvmStatic
        fun mostCommonSrc(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(1,2,1),
                Arguments.of(3,2,0)
            )
        }
    }
}