package diagnostics

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class FunctionsKtTest{
    @ParameterizedTest
    @MethodSource("countSrc")
    internal fun testCountOccurrences(string: String, char: Char, count: Int){
        assertEquals(count, countOccurrences(string, char))
    }

    @ParameterizedTest
    @MethodSource("countBitsSrc")
    internal fun testCountBits(string: String, zeroes: Int, ones: Int) {
        assertEquals(BitCount(zeroes, ones),string.countBits())
    }

    @Test
    internal fun testTranspose() {
        val matrix = listOf("foo", "bar", "bat")
            .map { it.toCharArray().toTypedArray() }
            .toTypedArray()

        val expected = arrayOf(
            arrayOf('f', 'b', 'b'),
            arrayOf('o', 'a', 'a'),
            arrayOf('o', 'r', 't'),
        )

        assertArrayEquals(expected, transpose(matrix))
    }

    companion object {
        @JvmStatic
        fun countSrc(): Stream<Arguments> {
            return Stream.of(
                Arguments.of("abcdefgh", '1', 0),
                Arguments.of("111", '1', 3),
                Arguments.of("11001", '1', 3),
                Arguments.of("00002341300", '0', 6)
            )
        }

        @JvmStatic
        fun countBitsSrc(): Stream<Arguments> {
            return Stream.of(
                Arguments.of("11111", 0, 5),
                Arguments.of("00000", 5, 0),
                Arguments.of("10101", 2, 3)
            )
        }
    }
}