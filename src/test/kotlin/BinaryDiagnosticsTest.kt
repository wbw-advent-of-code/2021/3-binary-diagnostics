import diagnostics.Diagnostics
import kotlin.test.Test
import kotlin.test.assertEquals

class BinaryDiagnosticsTest {
    private val fileName = "src/test/resources/input.txt"

    @Test
    internal fun part1() {
        assertEquals(198, Diagnostics().fromBinary(fileName).powerConsumption)
    }

    @Test
    internal fun part2() {
        assertEquals(230, Diagnostics().fromBinary(fileName).lifeSupportRating())
    }
}