package diagnostics

fun String.countBits():BitCount {
    return BitCount(
        zero = countOccurrences(this, '0'),
        one = countOccurrences(this, '1')
    )
}

fun countOccurrences(s: String, ch: Char): Int {
    return s.count { it == ch }
}

fun transpose(input: Array<Array<Char>>): Array<Array<Char>> {
    val transpose = Array(input[0].size){Array(input.size){' '} }

    for (i in input.indices) {
        for(j in 0 until input[0].size) {
            transpose[j][i] = input[i][j]
        }
    }

    return transpose
}

fun List<String>.countBitsInColumn(column: Int): BitCount {
    var zeroes = 0
    var ones = 0
    for(line in this) {
        if(line[column] == '0') zeroes++ else ones++
    }
    return BitCount(zeroes, ones)
}