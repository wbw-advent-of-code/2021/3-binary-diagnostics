package diagnostics

fun main() {
    val input = "src/main/resources/input.txt"
    println(Diagnostics().fromBinary(input).powerConsumption)
    println(Diagnostics().fromBinary(input).lifeSupportRating())
}