package diagnostics

import java.io.File

data class Diagnostics(
    val powerConsumption: Int = 0,
    val oxygenGeneratorRating: Int = 0,
    val co2ScrubberRating: Int = 0
) {
    fun fromBinary(fileName: String): Diagnostics {
        val binaryLines = File(fileName).useLines { it.toList() }

        return Diagnostics(
            powerConsumption(binaryLines),
            oxyRating(binaryLines),
            co2Rating(binaryLines)
        )
    }

    private fun oxyRating(binaryLines: List<String>): Int {
        return reduceCandidates(binaryLines, RatingType.OXY).toInt(2)
    }

    private fun co2Rating(binaryLines: List<String>): Int {
        return reduceCandidates(binaryLines, RatingType.CO2).toInt(2)
    }

    private fun reduceCandidates(binaryLines: List<String>, type: RatingType): String {
        val columns = binaryLines[0].indices
        var candidates = binaryLines
        for (col in columns) {
            val count = candidates.countBitsInColumn(col)
            val mostCommon = if (count.zero > count.one) '0' else '1'
            candidates = candidates.filter {
                when (type) {
                    RatingType.OXY -> it[col] == mostCommon
                    RatingType.CO2 -> it[col] != mostCommon
                }
            }

            if (candidates.size == 1) break
        }

        return candidates.single()
    }

    fun lifeSupportRating(): Int {
        return oxygenGeneratorRating * co2ScrubberRating
    }

    private fun powerConsumption(binaryLines: List<String>): Int {
        val matrix = binaryLines
            .map { it.toCharArray().toTypedArray() }
            .toTypedArray()
        val transposed = transpose(matrix)

        val gamma = transposed.toList()
            .map { String(it.toCharArray()) }
            .map { it.countBits().mostCommon() }
            .joinToString(separator = "") { it.toString() }
            .toInt(2)

        val epsilon = transposed.toList()
            .map { String(it.toCharArray()) }
            .map { it.countBits().leastCommon() }
            .joinToString(separator = "") { it.toString() }
            .toInt(2)

        val powerConsumption1 = gamma * epsilon
        return powerConsumption1
    }
}

data class BitCount(val zero: Int = 0, val one: Int = 0) {
    fun mostCommon(): Int {
        return if (zero > one) {
            0
        } else {
            1
        }
    }

    fun leastCommon(): Int {
        return if (zero > one) {
            1
        } else {
            0
        }
    }
}

enum class RatingType{
    OXY,
    CO2
}